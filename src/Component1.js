import React, { Component } from 'react';
import './App.css';

// UI framework component imports
import Appbar from 'muicss/lib/react/appbar';
import Button from 'muicss/lib/react/button';
import Container from 'muicss/lib/react/container';
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Textarea from 'muicss/lib/react/textarea';


export default class Component1 extends Component {

  // This component doesn't use any properties

  render() {
    const baseStyle = {};
    return (
      <div className="Component1" style={baseStyle}>
      </div>
    )
  }

}

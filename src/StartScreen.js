import React, { Component } from 'react';
import './App.css';
import CommentItem from './CommentItem';
import PostComment from './PostComment';

// UI framework component imports
import Appbar from 'muicss/lib/react/appbar';
import Button from 'muicss/lib/react/button';
import Container from 'muicss/lib/react/container';
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Textarea from 'muicss/lib/react/textarea';


export default class StartScreen extends Component {

  // Properties used by this component:
  // dataSheets, appActions, username

  render() {
    const baseStyle = {};
    const style_backgroundShape = {
        background: 'rgba(255, 255, 255, 1.000)',
        pointerEvents: 'none',
    
      };
    function transformPropValue_textBlock(input) {
      return "Hi "+input+"!";
    }
    const style_textBlock = {
        fontSize: 30.1,
        fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", sans-serif',
        fontWeight: 'bold',
        color: 'rgba(0, 0, 0, 0.5000)',
        textAlign: 'left',
        pointerEvents: 'none',
    
      };
    const value_textBlock = transformPropValue_textBlock(this.props.username ? this.props.username : '');
    
    const style_list = {
        height: 'auto',  // This element is in scroll flow
    
      };
    const dataSheets = this.props.dataSheets;
    return (
      <Container fluid={true} className="AppScreen StartScreen" style={baseStyle}>
        <div className="background">
          <div className='fullHeight elBackgroundShape' style={style_backgroundShape} />
        </div>
        <div className="layoutFlow">
          <div className='elTextBlock'>
            <div style={style_textBlock}>
              <div>{value_textBlock !== undefined ? value_textBlock : (<span className="propValueMissing">Hi Jane Doe!</span>)}</div>
            </div>
          
          </div>
          <div className='hasNestedComps elList'>
            <ul style={style_list}>
              {dataSheets['listData1'].items.map((row, index) => (
                <li key={row.key}><CommentItem username={row.username} text={row.text} appActions={this.props.appActions} /></li>
              ))}
            </ul>
          
          </div>
          <div className='hasNestedComps elPostComment'>
            <div>
              <PostComment appActions={this.props.appActions} />
            </div>
          
          </div>
        </div>
      </Container>
    )
  }

}

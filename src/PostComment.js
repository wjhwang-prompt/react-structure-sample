import React, { Component } from 'react';
import './App.css';

// UI framework component imports
import Appbar from 'muicss/lib/react/appbar';
import Button from 'muicss/lib/react/button';
import Container from 'muicss/lib/react/container';
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Textarea from 'muicss/lib/react/textarea';


export default class PostComment extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      username: '',
      text: '',
    };
  }

  // This component doesn't use any properties

  textInputChanged_username = (event) => {
    this.setState({username: event.target.value});
  }
  
  textInputChanged_text = (event) => {
    this.setState({text: event.target.value});
  }
  
  onClick_postButton = (ev) => {
    // Add row to sheet 'List data 1'
    let row = {
      ...this.state,
    }
    this.props.appActions.addToDataSheet('listData1', row);
  
  }
  
  
  render() {
    const baseStyle = {};
    const style_username = {
        display: 'block',
        paddingLeft: 8,
        backgroundColor: 'white',
    
      };
    const style_text = {
        display: 'block',
        paddingLeft: 8,
        backgroundColor: 'white',
    
      };
    const style_postButton = {
        display: 'block',
        color: 'white',
        textAlign: 'center',
        textTransform: 'none',
        cursor: 'pointer',
    
      };
    return (
      <div className="PostComment" style={baseStyle}>
        <div className="layoutFlow">
          <div className='elUsername'>
            <input style={style_username} type="text" placeholder="Your name..." onChange={this.textInputChanged_username} value={this.state.username}  />
          
          </div>
          <div className='elText'>
            <input style={style_text} type="text" placeholder="Say something!" onChange={this.textInputChanged_text} value={this.state.text}  />
          
          </div>
          <div className='actionFont elPostButton'>
            <Button style={style_postButton}  color="accent" onClick={this.onClick_postButton} >
              Post
            </Button>
          
          </div>
        </div>
      </div>
    )
  }

}

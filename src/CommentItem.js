import React, { Component } from 'react';
import './App.css';

// UI framework component imports
import Appbar from 'muicss/lib/react/appbar';
import Button from 'muicss/lib/react/button';
import Container from 'muicss/lib/react/container';
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Textarea from 'muicss/lib/react/textarea';


export default class CommentItem extends Component {

  // Properties used by this component:
  // username, text

  render() {
    const baseStyle = {};
    const style_username = {
        color: 'rgba(0, 0, 0, 0.8203)',
        textAlign: 'left',
        pointerEvents: 'none',
    
      };
    const value_username = this.props.username ? this.props.username : '';
    
    const style_line = {
        borderTop: '1px solid rgba(0, 0, 0, 0.2512)',
        pointerEvents: 'none',
    
      };
    const style_text = {
        color: 'rgba(0, 0, 0, 0.8203)',
        textAlign: 'left',
        pointerEvents: 'none',
    
      };
    const value_text = this.props.text ? this.props.text : '';
    
    return (
      <div className="CommentItem" style={baseStyle}>
        <div className="layoutFlow">
          <div className='headlineFont elUsername'>
            <div style={style_username}>
              <div>{value_username !== undefined ? value_username : (<span className="propValueMissing">[No value for prop: username]</span>)}</div>
            </div>
          
          </div>
          <div className='elLine'>
            <div style={style_line} />
          
          </div>
          <div className='elText'>
            <div style={style_text}>
              <div>{value_text !== undefined ? value_text : (<span className="propValueMissing">[No value for prop: text]</span>)}</div>
            </div>
          
          </div>
        </div>
      </div>
    )
  }

}

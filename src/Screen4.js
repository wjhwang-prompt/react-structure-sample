import React, { Component } from 'react';
import './App.css';

// UI framework component imports
import Appbar from 'muicss/lib/react/appbar';
import Button from 'muicss/lib/react/button';
import Container from 'muicss/lib/react/container';
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Textarea from 'muicss/lib/react/textarea';


export default class Screen4 extends Component {

  // Properties used by this component:
  // appActions

  render() {
    const baseStyle = {};
    return (
      <Container fluid={true} className="AppScreen Screen4" style={baseStyle}>
      </Container>
    )
  }

}

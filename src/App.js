import React, { Component } from 'react';
import './App.css';
import StartScreen from './StartScreen.js';
import Screen4 from './Screen4.js';
import Screen5 from './Screen5.js';
import Screen6 from './Screen6.js';
import DataSheet_listData1 from './DataSheet_listData1.js';


export default class App extends Component {
  constructor(props) {
    super(props);

    this.dataSheets = {};
    this.dataSheets['listData1'] = new DataSheet_listData1();

    this.dataSlots = {};
    this.dataSlots['username'] = "Jane Doe";
    this.dataSlots['userEmail'] = "";

    this.state = {
      currentScreen: 'start',
      currentScreenProps: {},
    }
    this.screenHistory = [ {...this.state} ];
  }

  goToScreen = (screenId, props) => {
    // This is the default implementation and could be customized by a navigation plugin.
    if ( !props)
      props = {};
    let screenState = {currentScreen: screenId, currentScreenProps: props};
    this.setState(screenState);
    this.screenHistory.push(screenState);
  }

  goBack = () => {
    // This is the default implementation and could be customized by a navigation plugin.
    if (this.screenHistory.length < 2)
      return;

    this.screenHistory.splice(this.screenHistory.length - 1, 1);
    let prevScreenState = this.screenHistory[this.screenHistory.length - 1];
    this.setState(prevScreenState);
  }

  addToDataSheet = (sheetId, row, actionId) => {
    // This is the default implementation and could be customized by a state management plugin.
    let sheet = this.dataSheets[sheetId];
    if (sheet && row) {
      sheet.addItem(row);
    }
    this.setState(this.state);
  }

  updateDataSlot = (slotId, value, actionId) => {
    // This is the default implementation and could be customized by a state management plugin.
    this.dataSlots[slotId] = value;
    this.setState(this.state);
  }

  render() {
    // Base app-level props passed to all screens
    let screenProps = {
      ...this.state.currentScreenProps,
      appActions: this,
      dataSheets: this.dataSheets,
      username: this.dataSlots['username'],
      userEmail: this.dataSlots['userEmail'],
    };
    let screenEl;
    switch (this.state.currentScreen) {
    case 'start':
      screenEl = (<StartScreen {...screenProps} />)
      break;
    case 'screen4':
      screenEl = (<Screen4 {...screenProps} />)
      break;
    case 'screen5':
      screenEl = (<Screen5 {...screenProps} />)
      break;
    case 'screen6':
      screenEl = (<Screen6 {...screenProps} />)
      break;
    }

    return (
      <div className="App">
        {screenEl}
      </div>
    );
  }
}
